package com.zebra.solutioncenter.zatarclient;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import android.os.Build;
import android.util.Log;

import com.zatar.device.Device;
import com.zatar.device.DeviceCommandStatus;
import com.zatar.device.DeviceException;
import com.zatar.device.DeviceIdentifier;
import com.zatar.device.Reporter;
import com.zatar.device.settings.DeviceStateAttribute;

public class MotorolaDevice implements Device {
	
	private Reporter reporter;
	String UID;
	private final Map<String, Integer> commandStateMap;
	private final MotorolaDeviceHeartbeatBehavior heartbeatBehavior;
    private final MotorolaDeviceDataBehavior dataBehavior;
	private DeviceIdentifier identifier;
    private Properties deviceProperties;
    private HashMap<Integer, String> state;
    private ZatarService zatarService;
	
	@Override
	public void connected(final Reporter reporter) {
		this.setReporter(reporter);
        zatarService.updateStatus(ZatarActivity.CLIENT_UPDATE.CONNECTED);
	}

	@Override
	public void disconnected() {
		this.setReporter(null);
        zatarService.updateStatus(ZatarActivity.CLIENT_UPDATE.DISCONNECTED);
	}

	public MotorolaDevice(Properties properties, ZatarService context) {
        this.deviceProperties = properties;
        this.zatarService = context;
		this.commandStateMap = new HashMap<String, Integer>();
        this.state = new HashMap<>();
        state.put(DeviceStateAttribute.FIRMWARE.getKey(), "1.01");
        state.put(DeviceStateAttribute.MANUFACTURER.getKey(), getDeviceProperties().getProperty(getZatarService().getString(R.string.pref_key_device_manufacturer)));
        state.put(DeviceStateAttribute.MODEL.getKey(), getDeviceProperties().getProperty(getZatarService().getString(R.string.pref_key_device_model)));
        state.put(DeviceStateAttribute.TYPE.getKey(), getDeviceProperties().getProperty(getZatarService().getString(R.string.pref_key_device_type)));
		heartbeatBehavior = new MotorolaDeviceHeartbeatBehavior(this);
        dataBehavior = new MotorolaDeviceDataBehavior(this);
	}
	
	@Override
	public Map<Integer, String> read(final Set<Integer> resourceIds)
			throws DeviceException {
		Map<Integer, String> readState = new HashMap<Integer, String>();
		if(resourceIds.isEmpty()){
			readState = getState();
		}
		else{
			for(final Integer resourceId : resourceIds){
				if(getState().containsKey(resourceId)){
					readState.put(resourceId, getState().get(resourceId));
					Log.i(MotorolaDevice.class.getName(), Build.SERIAL + " reading '" + getState().get(resourceId) + "' to Resource '" + resourceId + "'");
				}
			}
		}

		return readState;
	}


	public HashMap<Integer, String> getState() {
        try {
            if(getZatarService().getLocation() != null) {
                state.put(DeviceStateAttribute.GEO_LOCATION_LAT.getKey(), String.valueOf(getZatarService().getLocation().getLatitude()));
                state.put(DeviceStateAttribute.GEO_LOCATION_LON.getKey(), String.valueOf(getZatarService().getLocation().getLongitude()));
                state.put(DeviceStateAttribute.ALTITUDE.getKey(), String.valueOf(getZatarService().getLocation().getAltitude()));
                state.put(DeviceStateAttribute.SPEED.getKey(), String.valueOf(getZatarService().getLocation().getSpeed()));
            }
            state.put(DeviceStateAttribute.DATE_TIME.getKey(), Calendar.getInstance().toString());
            state.put(DeviceStateAttribute.IP_ADDRESS.getKey(), getLocalIpAddress());
        } catch (Exception e) {
            Log.e(MotorolaDevice.class.getName(), "Error when generating device state:");
            e.printStackTrace();
        }

		return this.state;
	}

	@Override
	public void write(final Map<Integer, String> resources) throws DeviceException {
		for(final Entry<Integer, String> resource : resources.entrySet()){
			if(!getState().containsKey(resource.getKey())){
				final String msg = "Trying to Write a Resource , " + resource.getKey() + ", this Device Does not have.";
				Log.e(MotorolaDevice.class.getName(), msg);
				throw new DeviceException(DeviceCommandStatus.FAILURE_INVALID_COMMAND, msg, new IllegalArgumentException(msg));
			}
			else{
				Log.i(MotorolaDevice.class.getName(), Build.SERIAL + " writing '" + resource.getValue() + "' to Resource '" + resource.getKey() + "'");
			}
		}
		
		getState().putAll(resources);
	}

	@Override
	public void execute(final int resourceId, final Map<String, String> params)
			throws DeviceException {
			Log.i(MotorolaDevice.class.getName(), Build.SERIAL + " executing on Resource '" + resourceId + "' with parameters " + params);
	}

	@Override
	public Map<String, Integer> getCommandResourceMap() {
		return getCommandStateMap();
	}

	public Map<String, Integer> getCommandStateMap() {
		return commandStateMap;
	}

	public MotorolaDeviceHeartbeatBehavior getHeartbeatBehavior() {
		return heartbeatBehavior;
	}

	public Reporter getReporter() {
		return reporter;
	}

	public void setReporter(Reporter reporter) {
		this.reporter = reporter;
	}

	public DeviceIdentifier getIdentifier() {

		return identifier;
	}

	public void setIdentifier(DeviceIdentifier identifier) {
		this.identifier = identifier;
	}

    public ZatarService getZatarService() {
        return zatarService;
    }

    public void setContext(ZatarService zatarService) {
        this.zatarService = zatarService;
    }

    public Properties getDeviceProperties() {
        return deviceProperties;
    }

    public void setDeviceProperties(Properties deviceProperties) {
        this.deviceProperties = deviceProperties;
    }

    public String getLocalIpAddress()
    {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (Exception ex) {
            Log.e("IP Address", ex.toString());
        }
        return null;
    }

    public MotorolaDeviceDataBehavior getDataBehavior() {
        return dataBehavior;
    }
}
