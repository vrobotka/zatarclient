package com.zebra.solutioncenter.zatarclient;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import com.zatar.client.Client;
import com.zatar.client.ClientException;
import com.zatar.util.thread.factory.DaemonThreadFactory;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

public class ZatarService extends Service {

    private static final int THREAD_POOL_SIZE = 1;
    private final ScheduledExecutorService executor;
    private final HashMap<Integer, String> state;

    private ZatarActivity.CLIENT_UPDATE currentStatus = ZatarActivity.CLIENT_UPDATE.UNKNOWN;
    private Location location;

    public final String DEVICE_PARAM_ID_KEY = "device.param.identifier";
    public final String DEVICE_PARAM_TYPE_KEY = "device.param.type";
    public final String DEVICE_PARAM_MANUFACTURER_KEY = "device.param.manufacturer";
    public SharedPreferences preferences;

	@Override
	public IBinder onBind(Intent intent) {
		
		return null;
	}

    protected void updateStatus(ZatarActivity.CLIENT_UPDATE status, Serializable extra) {
        this.currentStatus = status;
        Intent newIntent = new Intent();
        newIntent.setAction(ZatarActivity.ACTION_SERVICE_UPDATE);
        newIntent.putExtra(ZatarActivity.INTENT_STATUS_KEY, status);
        if(extra != null) {
            newIntent.putExtra(ZatarActivity.INTENT_EXTRA_KEY, extra);
        }
        sendBroadcast(newIntent);
    }

    protected void updateStatus(ZatarActivity.CLIENT_UPDATE status) {
        updateStatus(status, null);
    }

    public ZatarService() {
        executor = Executors.newScheduledThreadPool(THREAD_POOL_SIZE, new DaemonThreadFactory("Activity Thread"));
        state = new HashMap<Integer, String>();
    }

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        LocationListener locationListener = new ZatarLocationListener();
        if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, locationListener);
        }

	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
        updateStatus(ZatarActivity.CLIENT_UPDATE.STARTING);
        IntentFilter updateFilter = new IntentFilter(ZatarActivity.ACTION_SERVICE_REQUEST_UPDATE);
        registerReceiver(serviceUpdateReceiver, updateFilter);
        start();
		return START_STICKY;
	}

    @Override
	public void onDestroy() {
        updateStatus(ZatarActivity.CLIENT_UPDATE.STOPPING);
        unregisterReceiver(serviceUpdateReceiver);
        stop();
		super.onDestroy();
	}

    void addStringProperty(final Properties properties, int propertyID) {
        properties.put(getString(propertyID), preferences.getString(getString(propertyID), null));
    }

    void addIntegerProperty(final Properties properties, int propertyID) {
        properties.put(getString(propertyID), Integer.parseInt(preferences.getString(getString(propertyID), null)));
    }

    void addBooleanProperty(final Properties properties, int propertyID) {
        properties.put(getString(propertyID), preferences.getBoolean(getString(propertyID), false));
    }

    public void start() {
		final Properties properties = new Properties();
        Boolean useSerialAsUID = preferences.getBoolean(getString(R.string.pref_key_device_use_serial), true);
        final String deviceUID;
        if(useSerialAsUID) {
            deviceUID = Build.SERIAL;
        } else {
            deviceUID = preferences.getString(getString(R.string.pref_key_device_uid), null);
        }

		addStringProperty(properties, R.string.pref_key_device_manufacturer);
        addStringProperty(properties, R.string.pref_key_device_type);
        addStringProperty(properties, R.string.pref_key_server_address);
        addIntegerProperty(properties, R.string.pref_key_server_port);
        addIntegerProperty(properties, R.string.pref_key_client_buffer);
        addIntegerProperty(properties, R.string.pref_key_client_timeout);
        addBooleanProperty(properties, R.string.pref_key_client_ssl);
        addStringProperty(properties, R.string.pref_key_device_model);
        addStringProperty(properties, R.string.pref_key_client_type);
        properties.put(getString(R.string.pref_key_device_uid), deviceUID);
		final MotorolaDevice device = new MotorolaDevice(properties, this);
		try {
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					try {
                        String deviceID = properties.getProperty(getString(R.string.pref_key_device_uid));
                        String deviceType = properties.getProperty(getString(R.string.pref_key_client_type));
						Client.start(properties, device, deviceType, deviceUID);

                        updateStatus(ZatarActivity.CLIENT_UPDATE.STARTED);
					} catch (ClientException e) {
                        Log.i(ZatarService.class.getName(), "Couldn't start Zatar client");
						e.printStackTrace();
					}
				}
			}).start();

			startScheduledTasks(device);
			Log.i(ZatarService.class.getName(), "Zatar client started");
		} catch (final Exception e) {
			Log.e(ZatarService.class.getName(), "Unable to load client properties. Stopping", e);
		}
	}

    public void stop() {

        new Thread(new Runnable() {

            @Override
            public void run() {
                try {

                    executor.shutdownNow();
                    Client.stop();

                    updateStatus(ZatarActivity.CLIENT_UPDATE.STOPPED);
                } catch(final Exception e){
                    Log.e(ZatarService.class.getName(), "Issue while stopping.  Exiting Regardless.", e);
                    updateStatus(ZatarActivity.CLIENT_UPDATE.STOPPED);
                }
            }
        }).start();

        Log.i(ZatarService.class.getName(), "Shutting down");
    }

    private void startScheduledTasks(MotorolaDevice device) {
        final MotorolaDeviceHeartbeatBehavior behavior = device.getHeartbeatBehavior();
        final MotorolaDeviceDataBehavior dataBehavior = device.getDataBehavior();
        executor.scheduleWithFixedDelay(behavior, behavior.getInitialDelay(), behavior.getDelay(), behavior.getDelayTimeUnit());
        executor.scheduleWithFixedDelay(dataBehavior, dataBehavior.getInitialDelay(), dataBehavior.getDelay(), dataBehavior.getDelayTimeUnit());
    }

    BroadcastReceiver serviceUpdateReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            updateStatus(currentStatus);
        }
    };

    Map<Integer, Object> propertiesToMap(Properties properties) {
        Map<Integer, Object> map = new HashMap<Integer, Object>();
        addValueToMap(map, properties, R.string.pref_key_server_port);
        addValueToMap(map, properties, R.string.pref_key_device_uid);
        addValueToMap(map, properties, R.string.pref_key_device_manufacturer);
        addValueToMap(map, properties, R.string.pref_key_device_type);
        return map;
    }

    private void addValueToMap(Map<Integer,Object> map, Properties properties, Integer id) {
        map.put(id, properties.getProperty(getString(id)));
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }


    //---------------------- Location listener class
    private class ZatarLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location loc) {
            setLocation(loc);
        }

        @Override
        public void onProviderDisabled(String provider) {}

        @Override
        public void onProviderEnabled(String provider) {}

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    }

}
