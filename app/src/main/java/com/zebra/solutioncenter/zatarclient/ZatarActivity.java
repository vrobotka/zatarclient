package com.zebra.solutioncenter.zatarclient;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.Serializable;
import java.util.HashMap;

public class ZatarActivity extends Activity {

	protected static final String INTENT_STATUS_KEY = "status_key";
    protected static final String INTENT_EXTRA_KEY = "extra_key";

	public static final String ACTION_SERVICE_UPDATE = "com.zebra.solutioncenter.zatarclient.service_update";
    public static final String ACTION_SERVICE_REQUEST_UPDATE = "com.zebra.solutioncenter.zatarclient.request_service_update";
	public static final String ACTION_SERVICE_SEND_MESSAGE = "com.zebra.solutioncenter.zatarclient.service_update";


    public enum CLIENT_UPDATE {
        UNKNOWN,
        CONNECTED,
        DISCONNECTED,
        CONNECTING,
        STOPPING,
        SENDING,
        STARTING,
        STARTED,
        STOPPED,
        CONNECTION_ESTABLISHED,
        SENDING_NOTIFY,
        SENDING_HEARTBEAT,
        NOTIFY_SENT,
        HEARTBEAT_SENT,
        DISCOVERY,
        HEARTBEAT_FAILED,
        DISCOVERY_FAILED,
        NOTIFY_FAILED
    }

    private static final String COLOR_NEGATIVE = "red";
    private static final String COLOR_NEUTRAL = "white";
    private static final String COLOR_POSITIVE = "green";
    private static final String COLOR_COMMUNICATION = "#506791";
    //private static final String COLOR_GREEN = "green";


    private static final int MAX_LOG_SIZE = 1024;
    private static final int MAX_LOG_LINES = 50;
	
	private CLIENT_UPDATE status = CLIENT_UPDATE.UNKNOWN;

    private String html = "";

	@Override
	protected void onStop() {
		super.onStop();
		unregisterReceiver(serviceUpdateReceiver);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		IntentFilter updateFilter = new IntentFilter(ACTION_SERVICE_UPDATE);
		registerReceiver(serviceUpdateReceiver, updateFilter);
	}

    @Override
    protected void onResume() {
        super.onResume();
        Intent newIntent = new Intent();
        newIntent.setAction(ZatarActivity.ACTION_SERVICE_REQUEST_UPDATE);
        sendBroadcast(newIntent);
    }

    /** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_main);
        if(android.os.Build.VERSION.SDK_INT > 14) {
            ActionBar actionBar = getActionBar();
            if (actionBar != null) {
                //actionBar.setDisplayOptions(ActionBar.DISPLAY_USE_LOGO);
                actionBar.setDisplayShowTitleEnabled(false);
                //            actionBar.setBackgroundDrawable(new ColorDrawable(0xffca33));
            }
        }
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
	    Button btnStartStop = (Button) findViewById(R.id.btnStartStop);
	    btnStartStop.setOnClickListener(btnStartStopListener);
	}

	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch(id) {
            case R.id.action_settings:
                startActivity(new Intent(this, ZatarSettingsActivity.class));
                return true;
            case R.id.action_show_serial:
                new AlertDialog.Builder(this)
                    .setTitle("Device UID")
                    .setMessage(Build.SERIAL)
                    .setPositiveButton(android.R.string.ok, null)
                    .show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    View.OnClickListener btnStartStopListener = new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			switch (status) {
                case DISCONNECTED:
                case STOPPED:
                case UNKNOWN:
                    startService(new Intent(getApplicationContext(), ZatarService.class));
                    break;
				case CONNECTED:
                case STARTING:
				case CONNECTING:
				case SENDING:
                case CONNECTION_ESTABLISHED:
                case STARTED:
                default:
					stopService(new Intent(getApplicationContext(), ZatarService.class));
					break;
			}
		}
	}; 
	
	BroadcastReceiver serviceUpdateReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			status = (CLIENT_UPDATE)intent.getExtras().get(INTENT_STATUS_KEY);
            Serializable extras = intent.getExtras().getSerializable(INTENT_EXTRA_KEY);
			updateStatus(status, extras);
		}
	};

    private void updateTextViews(String text, String color) {
        TextView tvStatus = (TextView) findViewById(R.id.tvStatus);
        tvStatus.setText(text);
        addLogLine(text, color);
    }

	private void updateStatus(CLIENT_UPDATE status) {
        updateStatus(status, null);
    }

    private void updateStatus(CLIENT_UPDATE status, Serializable extra) {
		ImageView imgZatarLogo = (ImageView) findViewById(R.id.imgZatarLogo);
        ImageView imgArrows = (ImageView) findViewById(R.id.btn_arrows);
        ImageView imgDevice = (ImageView) findViewById(R.id.img_device);

		Button btnStartStop = (Button) findViewById(R.id.btnStartStop);

		switch (status) {
		case UNKNOWN:
			btnStartStop.setBackgroundResource(R.drawable.btn_green);
			btnStartStop.setText("Start client");
			updateTextViews("Client is not running", COLOR_NEUTRAL);
			imgZatarLogo.setColorFilter(0xaa555555);
            imgArrows.setImageResource(R.drawable.arrows_grey);
            imgDevice.setImageResource(R.drawable.android_phone_black);
			break;
        case CONNECTION_ESTABLISHED:
            btnStartStop.setBackgroundResource(R.drawable.btn_red);
            btnStartStop.setText("Stop client");
            updateTextViews("Two-way connection with Zatar established", COLOR_POSITIVE);
            imgZatarLogo.setColorFilter(0x00555555);
            imgArrows.setImageResource(R.drawable.arrows_green);
            imgDevice.setImageResource(R.drawable.android_phone_green);
            break;
        case STARTING:
            btnStartStop.setBackgroundResource(R.drawable.btn_grey);
            btnStartStop.setText("Cancel");
            updateTextViews("Client is starting", COLOR_NEUTRAL);
            imgZatarLogo.setColorFilter(0xaa555555);
            imgArrows.setImageResource(R.drawable.arrows_grey);
            imgDevice.setImageResource(R.drawable.android_phone_red);
            break;
        case STARTED:
            btnStartStop.setText("Stop client");
            btnStartStop.setBackgroundResource(R.drawable.btn_red);
            updateTextViews("Client is started", COLOR_POSITIVE);
            imgZatarLogo.setColorFilter(0xaa555555);
            imgArrows.setImageResource(R.drawable.arrows_grey);
            imgDevice.setImageResource(R.drawable.android_phone_green);
            break;
		case CONNECTED:
			btnStartStop.setText("Stop client");
            btnStartStop.setBackgroundResource(R.drawable.btn_red);
			updateTextViews("Client is connected", COLOR_POSITIVE);
			imgZatarLogo.setColorFilter(0x00555555);
            imgArrows.setImageResource(R.drawable.arrows_bottom);
            imgDevice.setImageResource(R.drawable.android_phone_green);
			break;
		case DISCONNECTED:
			btnStartStop.setText("Restart");
            btnStartStop.setBackgroundResource(R.drawable.btn_grey);
			updateTextViews("Client has disconnected", COLOR_NEGATIVE);
			imgZatarLogo.setColorFilter(0xaa555555);
            imgArrows.setImageResource(R.drawable.arrows_grey);
            imgDevice.setImageResource(R.drawable.android_phone_green);
			break;
		case CONNECTING:
            btnStartStop.setText("Cancel");
            btnStartStop.setBackgroundResource(R.drawable.btn_grey);
			updateTextViews("Client is connecting", COLOR_NEUTRAL);
			imgZatarLogo.setColorFilter(0xaa555555);
            imgArrows.setImageResource(R.drawable.arrows_grey);
            imgDevice.setImageResource(R.drawable.android_phone_green);
			break;
		case STOPPING:
            btnStartStop.setText("Cancel");
            btnStartStop.setBackgroundResource(R.drawable.btn_grey);
			updateTextViews("Client is stopping", COLOR_NEUTRAL);
			imgZatarLogo.setColorFilter(0x00555555);
            imgArrows.setImageResource(R.drawable.arrows_top);
            imgDevice.setImageResource(R.drawable.android_phone_red);
			break;
        case STOPPED:
            btnStartStop.setText("Start client");
            btnStartStop.setBackgroundResource(R.drawable.btn_green);
            updateTextViews("Client has stopped", COLOR_POSITIVE);
            imgZatarLogo.setColorFilter(0xaa555555);
            imgArrows.setImageResource(R.drawable.arrows_grey);
            imgDevice.setImageResource(R.drawable.android_phone_black);
            break;
        case DISCOVERY:
            updateTextViews("Sending Discovery", COLOR_COMMUNICATION);
            break;
        case DISCOVERY_FAILED:
            updateTextViews("Discovery failed", COLOR_NEGATIVE);
            break;
        case SENDING_HEARTBEAT:
            updateTextViews("Sending Heartbeat", COLOR_COMMUNICATION);
            break;
        case HEARTBEAT_SENT:
            updateTextViews("Heartbeat sent", COLOR_POSITIVE);
            break;
        case HEARTBEAT_FAILED:
            updateTextViews("Sending Heartbeat failed", COLOR_NEGATIVE);
            break;
        case SENDING_NOTIFY:
            updateTextViews("Sending Notify", COLOR_COMMUNICATION);
            if(extra != null) {
                addLogLine(extra.toString(), COLOR_COMMUNICATION);
            }
            break;
        case NOTIFY_SENT:
            updateTextViews("Notify sent", COLOR_POSITIVE);
            break;
        case NOTIFY_FAILED:
            updateTextViews("Sending Notify failed", COLOR_NEGATIVE);
            break;

            default:
			break;
		}
	}

    private void addLogLine(final String string, final String color){
        final TextView tvLog = (TextView) findViewById(R.id.tvLog);
        final ScrollView mScrollView = (ScrollView) findViewById(R.id.scrollText);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(html.split("<br>").length > MAX_LOG_LINES) {
                   html = html.substring(html.indexOf("<br>") + 4);
                }
                html = html + "<font color=\"" + color + "\">" + string + "</font><br>";
                tvLog.setText(Html.fromHtml(html));

                mScrollView.post(new Runnable() {
                    public void run()
                    {
                        mScrollView.smoothScrollTo(0, tvLog.getBottom());
                    }
                });
            }
        });
    }
}
