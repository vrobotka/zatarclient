package com.zebra.solutioncenter.zatarclient;

import java.util.concurrent.TimeUnit;

import android.util.Log;

import com.zatar.device.DeviceIdentifier;
import com.zatar.device.Reporter;

public class MotorolaDeviceHeartbeatBehavior implements Runnable {

	protected static final long INITIAL_DELAY = 200;
	protected static final long DELAY_10_SEC = 10000;

	private final MotorolaDevice motorolaDevice;

	public MotorolaDeviceHeartbeatBehavior(MotorolaDevice motorolaDevice) {
		this.motorolaDevice = motorolaDevice;
	}

	@Override
	public void run() {
		runActivity(motorolaDevice.getReporter());
	}
	
	protected final MotorolaDevice getDevice() {
		return  motorolaDevice;
	}
	


	public long getInitialDelay(){
		return INITIAL_DELAY;
	}


	public TimeUnit getDelayTimeUnit() {
		return TimeUnit.MILLISECONDS;
	}
	
	public void runActivity(Reporter reporter) {
		Log.i(MotorolaDevice.class.getName(), "Sending Heartbeat...");
        getDevice().getZatarService().updateStatus(ZatarActivity.CLIENT_UPDATE.SENDING_HEARTBEAT);
		if(reporter != null && getDevice().getIdentifier() != null){
			try{
			reporter.heartbeat(getDevice().getIdentifier());
			Log.i(MotorolaDeviceHeartbeatBehavior.class.getName(), "Heartbeat sent");
            getDevice().getZatarService().updateStatus(ZatarActivity.CLIENT_UPDATE.HEARTBEAT_SENT);
			}
			catch(Exception e){
				Log.e(MotorolaDeviceHeartbeatBehavior.class.getName(), "Unable to send heartbeat", e);
                getDevice().getZatarService().updateStatus(ZatarActivity.CLIENT_UPDATE.HEARTBEAT_FAILED);
			}
		}
		else {
			Log.i(MotorolaDeviceHeartbeatBehavior.class.getName(), "Zatar Client Not ready to send Heartbeat {" + reporter + ", " + getDevice().getIdentifier() + "}");
            getDevice().getZatarService().updateStatus(ZatarActivity.CLIENT_UPDATE.HEARTBEAT_FAILED);
            if(reporter != null) {
                runDiscoveryActivity(reporter);
            }
		}
	}

    private void runDiscoveryActivity(final Reporter reporter) {
        try {
            Log.i(MotorolaDevice.class.getName(), "Discovering");
            getDevice().getZatarService().updateStatus(ZatarActivity.CLIENT_UPDATE.DISCOVERY);
            final DeviceIdentifier deviceIdentifier = reporter.discover(
                    getDevice().getLocalIpAddress(),
                    (Integer)getDevice().getDeviceProperties().get(getDevice().getZatarService().getString(R.string.pref_key_server_port)),
                    (String)getDevice().getDeviceProperties().get(getDevice().getZatarService().getString(R.string.pref_key_device_uid)),
                    (String)getDevice().getDeviceProperties().get(getDevice().getZatarService().getString(R.string.pref_key_client_type)),
                    (String)getDevice().getDeviceProperties().get(getDevice().getZatarService().getString(R.string.pref_key_device_manufacturer)));

            getDevice().getZatarService().updateStatus(ZatarActivity.CLIENT_UPDATE.CONNECTION_ESTABLISHED);

            getDevice().setIdentifier(deviceIdentifier);

        } catch (final Exception e) {
            Log.e(MotorolaDeviceHeartbeatBehavior.class.getName(), "Error trying to generate IP Address for Device during Discovery", e);
            getDevice().getZatarService().updateStatus(ZatarActivity.CLIENT_UPDATE.DISCOVERY_FAILED);
        }
    }

	public long getDelay() {
		return DELAY_10_SEC;
	}
}
