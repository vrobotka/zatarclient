package com.zebra.solutioncenter.zatarclient;

import android.util.Log;

import com.zatar.device.DeviceIdentifier;
import com.zatar.device.Reporter;

import java.util.concurrent.TimeUnit;

public class MotorolaDeviceDataBehavior implements Runnable {

	protected static final long INITIAL_DELAY = 200;
	protected static final long DELAY_30_SEC = 30000;

	private final MotorolaDevice motorolaDevice;

	public MotorolaDeviceDataBehavior(MotorolaDevice motorolaDevice) {
		this.motorolaDevice = motorolaDevice;
	}

	@Override
	public void run() {
		runActivity(motorolaDevice.getReporter());
	}
	
	protected final MotorolaDevice getDevice() {
		return  motorolaDevice;
	}
	


	public long getInitialDelay(){
		return INITIAL_DELAY;
	}


	public TimeUnit getDelayTimeUnit() {
		return TimeUnit.MILLISECONDS;
	}
	
	public void runActivity(Reporter reporter) {
		Log.i(MotorolaDevice.class.getName(), "Sending Notify...");
        getDevice().getZatarService().updateStatus(ZatarActivity.CLIENT_UPDATE.SENDING_NOTIFY, getDevice().getState());

		if(reporter != null && getDevice().getIdentifier() != null){
			try{
                reporter.notify(getDevice().getIdentifier(), getDevice().getState());
                Log.i(MotorolaDeviceDataBehavior.class.getName(), "Notify sent");
                getDevice().getZatarService().updateStatus(ZatarActivity.CLIENT_UPDATE.NOTIFY_SENT);
			}
			catch(Exception e){
				Log.e(MotorolaDeviceDataBehavior.class.getName(), "Unable to send heartbeat", e);
			}
		}
	}

	public long getDelay() {
		return DELAY_30_SEC;
	}



}
